﻿using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    private static DialogueManager _dialogueManager;

    [SerializeField] private TextMeshProUGUI _dialogueText;
    private static Tween _clearTween;
    private static Action _onDialogueEndAction;

    private static List<DialogueText> _textQueue;

    private void Start()
    {
        _dialogueManager = this;
        FinishDialogue();
    }

    public static void FinishDialogue()
    {
        PlayerController.Interactable = false;
        _dialogueManager.SetDialogueText(string.Empty);
    }

    public static void SetText(DialogueText text)
    {
        PlayerController.Interactable = false;

        if (_clearTween != null && _clearTween.IsPlaying())
            _clearTween.Kill();

        DOVirtual.DelayedCall(text.StartDelay, () =>
        {
            _dialogueManager.SetDialogueText(text.Text);
            if(text.Voiceover != null)
                text.Voiceover.Play();
        });

        var showTime = text.TextDuration + text.StartDelay;
        _clearTween = DOVirtual.DelayedCall(showTime, OnEndDialogue);
    }

    private static void OnEndDialogue()
    {
        FinishDialogue();

        if (_textQueue.Count > 0)
            SetNextTextFromQueue();
        else
        {
            if (_onDialogueEndAction != null)
            {
                _onDialogueEndAction.Invoke();
                _onDialogueEndAction = null;
            }
            PlayerController.Interactable = true;
        }
    }

    private static void SetNextTextFromQueue()
    {
        var firstText = _textQueue.First();
        _textQueue.RemoveAt(0);
        SetText(firstText);
    }

    public static void SetText(List<DialogueText> textList, Action onDialogueEndAction = null)
    {
        _onDialogueEndAction = onDialogueEndAction;
        _textQueue = textList.ToList();
        SetNextTextFromQueue();
    }

    public void SetDialogueText(string text)
    {
        _dialogueManager._dialogueText.text = text;
    }

}
