﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    public Image Fader;
    public TextMeshProUGUI Title;
    public TextMeshProUGUI EndTitleText;

    public List<EndTitle> EndTitles;

    public void StartEnding()
    {
        var endTitles = DOTween.Sequence();
        var fadeOut = Fader.DOFade(1f, 5f).SetDelay(3f);
        endTitles.Append(fadeOut);
        var title = Title.DOFade(1f, 3f).SetLoops(2, LoopType.Yoyo);
        endTitles.Append(title);
        endTitles.AppendInterval(1f);

        foreach (var endTitle in EndTitles)
        {
            var text = string.Format("<#FF0000FF>{0}</color>\n{1}", endTitle.Description, endTitle.Name);
            endTitles.AppendCallback(() => EndTitleText.text = text);
            var endTitleTween = EndTitleText.DOFade(1f, 3f).SetLoops(2, LoopType.Yoyo);
            endTitles.Append(endTitleTween);
            endTitles.AppendInterval(1f);
        }
    }

    [Serializable]
    public class EndTitle
    {
        public string Description;
        public string Name;
    }
}
