﻿using System.Collections.Generic;
using DefaultNamespace;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameInitializer : MonoBehaviour
{
    public Image FadeOverlay;
    public List<DialogueText> StartingDialogue;
    public AudioSource Telefon;
    public TextMeshProUGUI Title;
    public AudioSource Thunder;
    
	void Start ()
	{
	    FadeOverlay.color = Color.black;
        var intro = DOTween.Sequence();
	    intro.AppendInterval(1f);
	    //var thunder = FadeOverlay.DOColor(Color.white, 0.1f).SetLoops(2, LoopType.Yoyo).SetDelay(3f).OnStart(() => Thunder.Play());
	    //intro.Join(thunder);
        var title = Title.DOFade(1f, 3f).SetLoops(2, LoopType.Yoyo);
        intro.Append(title);
	    var fadeout = FadeOverlay.DOFade(0f, 3f).SetEase(Ease.OutQuad);
	    intro.Append(fadeout);
	    intro.AppendCallback(() => DialogueManager.SetText(StartingDialogue));
	    var telefon = DOVirtual.DelayedCall(3f, () => Telefon.Play());
	    intro.Join(telefon);
    }
}
