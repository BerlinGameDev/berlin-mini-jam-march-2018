﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Audio;

public class PutPlayerInCloset : EndAction
{
    public Transform Player;
    public SpriteRenderer PlayerSpriteRenderer;
    public string PlayerSortingLayer;
    public Transform PlayerInClosetPosition;
    public EndAction OpenDoorAnimation;
    public EndAction CloseDoorSlowly;
    public EndGame EndGame;
    public AudioMixerSnapshot FinalSnapshot;
    public AudioSource FinalMusic;

    public override void DoSomething()
    {
        PlayerController.Interactable = false;
        OpenDoorAnimation.DoSomething();
        FinalMusic.Play();
        FinalSnapshot.TransitionTo(3f);

        var sequence = DOTween.Sequence();
        sequence.AppendInterval(0.7f);
        sequence.AppendCallback(() =>
        {
            Player.position = PlayerInClosetPosition.position;
            PlayerSpriteRenderer.sortingLayerName = PlayerSortingLayer;
        });
        sequence.AppendInterval(0.5f);
        sequence.AppendCallback(CloseDoorSlowly.DoSomething);
        sequence.AppendCallback(EndGame.StartEnding);
    }
}
