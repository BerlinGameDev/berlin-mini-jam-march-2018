﻿using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

public class NextHole : EndAction
{
    public List<GameObject> Holes;
    private int _currentHole = 0;
	
    public override void DoSomething()
    {
        ShowNextHole();
    }

    private void ShowNextHole()
    {
        Holes[_currentHole].SetActive(false);
        _currentHole++;
        Holes[_currentHole].SetActive(true);
        if (_currentHole == Holes.Count - 1)
            EnablePortalToRedRoom();
    }

    private void EnablePortalToRedRoom()
    {
        
    }
}
