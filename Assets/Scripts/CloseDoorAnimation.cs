﻿using UnityEngine;

namespace DefaultNamespace
{
    public class CloseDoorAnimation : EndAction
    {
        public Animator ClosetAnimator;

        public override void DoSomething()
        {
            ClosetAnimator.SetBool("Open", false);
        }
    }
}