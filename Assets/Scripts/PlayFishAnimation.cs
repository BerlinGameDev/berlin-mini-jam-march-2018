﻿using DG.Tweening;
using UnityEngine;

namespace DefaultNamespace
{
    public class PlayFishAnimation : EndAction
    {
        public Animator fishAnimator;
        public AudioSource DropSound;
        public Transform kartina_dark;
        public Vector2 kartinaDropPosition;
        public override void DoSomething()
        {
            fishAnimator.SetBool("Fall", true);
            DOVirtual.DelayedCall(0.7f, ()=> DropSound.Play());
            kartina_dark.localPosition = kartinaDropPosition;
        }
    }
}