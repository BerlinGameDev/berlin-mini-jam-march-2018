﻿using System;
using System.Collections.Generic;
using DefaultNamespace;
using DG.Tweening;
using UnityEngine;

public class ClickableTarget : MonoBehaviour
{
    [Serializable]
    public class EnableDisableClickableTarget
    {
        public bool Disable = true;
        public ClickableTarget ClickableTarget;
    }

    [Serializable]
    public class EnableDisableObject
    {
        public bool Disable = true;
        public GameObject GameObject;
    }

    [Serializable]
    public class PlayStopSound
    {
        public bool Play = true;
        public AudioSource Audio;
    }

    public bool Enabled = true;
    public List<DialogueText> Dialogues;
    public EndAction OnDialogueEnd;

    public List<EnableDisableClickableTarget> EnableDisableClickableTargetList;
    public List<EnableDisableObject> EnableDisableObjectList;
    public List<PlayStopSound> PlayStopSoundList;
    public EndAction StartAction;

    public virtual void Click()
    {
        Action action = null;
        if (OnDialogueEnd != null)
            action = OnDialogueEnd.DoSomething;
        if (Dialogues.Count > 0)
            DialogueManager.SetText(Dialogues, action);

        EnableDisableObjects();
        EnableDisableClickableTargets();
        PlayStopSounds();
        if(StartAction != null)
            StartAction.DoSomething();
    }

    private void PlayStopSounds()
    {
        foreach (var sound in PlayStopSoundList)
        {
            if (sound.Audio == null)
                return;
            if(sound.Play)
                sound.Audio.Play();
            else
                sound.Audio.Stop();
        }
    }

    private void EnableDisableClickableTargets()
    {
        foreach (var Target in EnableDisableClickableTargetList)
        {
            if (Target.ClickableTarget != null)
                Target.ClickableTarget.Enabled = !Target.Disable;
        }
    }

    private void EnableDisableObjects()
    {
        foreach (var GO in EnableDisableObjectList)
        {
            if(GO.GameObject != null)
                GO.GameObject.SetActive(!GO.Disable);
        }
    }

    void OnMouseDown()
    {
        if (!Enabled || !PlayerController.Interactable)
            return;

        PlayerController.MoveAndClick(this);
    }
}