﻿using UnityEngine;

public abstract class EndAction : MonoBehaviour
{
    public abstract void DoSomething();
}