﻿using DG.Tweening;
using UnityEngine;

namespace DefaultNamespace
{
    public class ThrowMonkeyInTheCloset : EndAction
    {
        public Transform Monkey;
        public EndAction OpenCloset;
        public EndAction CloseCloset;
        public AudioSource MonkeySound1;
        public AudioSource MonkeySound2;
        public SpriteRenderer Gnome;
        public DialogueText GnomeDialogue;
        public EndAction UnlockNextHole;
        public GameObject Skeleton;

        public override void DoSomething()
        {
            PlayerController.Interactable = false;
            OpenCloset.DoSomething();
            var sequence = DOTween.Sequence();
            sequence.AppendInterval(0.7f);
            sequence.AppendCallback(() => Monkey.gameObject.SetActive(true));
            sequence.AppendInterval(0.3f);
            sequence.AppendCallback(() => CloseCloset.DoSomething());
            sequence.AppendInterval(0.5f);
            sequence.AppendCallback(() =>
            {
                MonkeySound1.Play();
                MonkeySound2.Play();
            });
            sequence.AppendInterval(3f);
            sequence.AppendCallback(() => OpenCloset.DoSomething());
            sequence.AppendInterval(0.5f);
            sequence.AppendCallback(() => DialogueManager.SetText(GnomeDialogue));
            sequence.AppendInterval(2f);
            var gnomeFade = Gnome.DOFade(0f, 1f);
            sequence.Append(gnomeFade);
            sequence.AppendCallback(() => CloseCloset.DoSomething());
            sequence.AppendInterval(1f);
            sequence.AppendCallback(() =>
            {
                Monkey.gameObject.SetActive(false);
                Skeleton.SetActive(true);
                UnlockNextHole.DoSomething();
                PlayerController.Interactable = true;
            });
        }
    }
}