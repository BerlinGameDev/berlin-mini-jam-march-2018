﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    [Serializable]
    public class DialogueText
    {
        public string Text;
        public float TextDuration = 3f;
        public float StartDelay = 0f;
        public AudioSource Voiceover;
    }
}