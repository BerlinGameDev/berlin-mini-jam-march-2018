﻿using UnityEngine;

namespace DefaultNamespace
{
    public class OpenDoorAnimation : EndAction
    {
        public Animator ClosetAnimator;

        public override void DoSomething()
        {
            ClosetAnimator.SetBool("Open", true);
        }
    }
}