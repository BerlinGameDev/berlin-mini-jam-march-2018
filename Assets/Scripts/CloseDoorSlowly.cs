﻿using UnityEngine;

namespace DefaultNamespace
{
    public class CloseDoorSlowly : EndAction
    {
        public Animator ClosetAnimator;

        public override void DoSomething()
        {
            ClosetAnimator.SetTrigger("CloseSlowly");
        }
    }
}