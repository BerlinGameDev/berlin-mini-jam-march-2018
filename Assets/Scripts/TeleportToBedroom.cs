﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class TeleportToBedroom : EndAction
    {
        public Image Fader;
        public GameObject Bedroom;
        public GameObject RedRoom;

        public override void DoSomething()
        {
            PlayerController.Interactable = false;
            var sequence = DOTween.Sequence();
            var fadeOut = Fader.DOFade(1f, 1.5f);
            var fadeIn = Fader.DOFade(0f, 1.5f);
            sequence.Append(fadeOut);
            sequence.AppendCallback(() =>
            {
                Bedroom.SetActive(true);
                RedRoom.SetActive(false);
            });
            sequence.Append(fadeIn);
            sequence.AppendCallback(() => PlayerController.Interactable = true);
        }
    }
}