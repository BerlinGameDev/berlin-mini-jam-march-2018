﻿using Gamekit2D;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static bool Interactable;

    private const float Gravity = 5;
    private readonly int m_HashHorizontalSpeedPara = Animator.StringToHash("HorizontalSpeed");

    private static PlayerController _playerController;

    [SerializeField] private AudioSource _steps;
    [SerializeField] private CharacterController2D _characterController;
    [SerializeField] private SpriteRenderer _spriteRenderer;
    [SerializeField] private Animator _animator;
    [SerializeField] private float _movementSpeed = 1f;
    [SerializeField] private float _stopDistanceBeforeTarget = 2f;
    private static Camera _camera;
    private float _xDestination;
    private Vector2 _moveVector;

    private static ClickableTarget _clickableTarget;

    private void Start ()
    {
        _camera = Camera.main;
	}

    private void OnEnable()
    {
        _playerController = this;
    }

    private void Move(Vector2 destinationPosition)
    {
        _moveVector = (destinationPosition - PlayerPosition);
        _moveVector.x = _moveVector.x < 0 ? -1f : 1f;
        _xDestination = destinationPosition.x - _stopDistanceBeforeTarget * _moveVector.x;
        _animator.SetFloat(m_HashHorizontalSpeedPara, _moveVector.x);
        _spriteRenderer.flipX = _moveVector.x < 0;
    }

    public static void MoveAndClick(ClickableTarget clickableTarget)
    {
        _clickableTarget = clickableTarget;
        var clickPosition = (Vector2)_camera.ScreenToWorldPoint(Input.mousePosition);
        _playerController.Move(clickPosition);
    }

    private void Stop()
    {
        _steps.Stop();
        _moveVector = Vector2.zero;
        _animator.SetFloat(m_HashHorizontalSpeedPara, _moveVector.x);

        if (_clickableTarget != null)
        {
            _clickableTarget.Click();
            _clickableTarget = null;
        }
    }

    void Update()
    {
        if(Mathf.Abs(_moveVector.x) > 0 && !_steps.isPlaying)
            _steps.Play();
    }

    void FixedUpdate()
    {
        if (!Interactable)
            return;
        _moveVector.y = -Gravity;
        _characterController.Move(_moveVector * _movementSpeed * Time.deltaTime);

        if(Mathf.Abs(_moveVector.x) > 0 && PlayerReachedDestination)
            Stop();
    }

    private Vector2 PlayerPosition
    {
        get { return _characterController.Rigidbody2D.position; }
    }

    private bool PlayerReachedDestination
    {
        get
        {
            if(_moveVector.x <= 0 && PlayerPosition.x < _xDestination)
                return true;
            else if (_moveVector.x >= 0 && PlayerPosition.x > _xDestination)
                return true;

            return false;
        }
    }
}
