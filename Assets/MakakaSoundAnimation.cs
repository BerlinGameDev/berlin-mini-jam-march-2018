﻿using DefaultNamespace;
using DG.Tweening;
using UnityEngine;

public class MakakaSoundAnimation : EndAction
{
    public Transform MakakaTransform;

    public override void DoSomething()
    {
        MakakaTransform.DOShakePosition(2f, 0.3f);
    }
}
